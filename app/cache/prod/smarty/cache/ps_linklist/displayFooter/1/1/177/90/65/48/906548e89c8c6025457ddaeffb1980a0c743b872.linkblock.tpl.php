<?php /*%%SmartyHeaderCode:1327871425b0feebd551b64-67304523%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:ps_linklist/views/templates/hook/linkblock.tpl',
      1 => 1527169470,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '1327871425b0feebd551b64-67304523',
  'variables' => 
  array (
    'linkBlocks' => 0,
    'linkBlock' => 0,
    '_expand_id' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5b0feebd5a9ce6_16014106',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b0feebd5a9ce6_16014106')) {function content_5b0feebd5a9ce6_16014106($_smarty_tpl) {?><div class="col-md-4 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <h3 class="h3 hidden-sm-down">Товары</h3>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_6584" data-toggle="collapse">
        <span class="h3">Товары</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_6584" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://poshta.test/prices-drop"
                title="Our special products"
                            >
              Снижение цен
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://poshta.test/new-products"
                title="Наши новинки"
                            >
              Новые товары
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://poshta.test/best-sales"
                title="Наши лидеры продаж"
                            >
              Лидеры продаж
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <h3 class="h3 hidden-sm-down">Наша компания</h3>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_19463" data-toggle="collapse">
        <span class="h3">Наша компания</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_19463" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://poshta.test/content/1-delivery"
                title="Сроки и условия доставки"
                            >
              Доставка
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://poshta.test/content/2-legal-notice"
                title="Правовое положениe"
                            >
              Правовое положение
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://poshta.test/content/3-terms-and-conditions-of-use"
                title="Порядок и условия использования"
                            >
              Порядок и условия использования
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://poshta.test/content/4-about-us"
                title="Информация о компании"
                            >
              О компании
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://poshta.test/content/5-secure-payment"
                title="Безопасность платежей"
                            >
              Безопасность платежей
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://poshta.test/contact-us"
                title="Для связи с нами используйте контактную форму"
                            >
              Свяжитесь с нами
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://poshta.test/карта сайта"
                title="Потерялись? Используйте поиск"
                            >
              Карта сайта
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://poshta.test/stores"
                title=""
                            >
              Магазины
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }} ?>
