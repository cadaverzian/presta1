<?php /*%%SmartyHeaderCode:11256614705b0feebcc5a403-00574772%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:ps_featuredproducts/views/templates/hook/ps_featuredproducts.tpl',
      1 => 1527169470,
      2 => 'module',
    ),
    'bda9c7c4d6fc15a2e7b9562908fa1cda4b56cb67' => 
    array (
      0 => '/home/poshta/public_html/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1527169470,
      2 => 'file',
    ),
    '45838c20ee241c7f295d17273948c62daec99df0' => 
    array (
      0 => '/home/poshta/public_html/themes/classic/templates/catalog/_partials/variant-links.tpl',
      1 => 1527169470,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11256614705b0feebcc5a403-00574772',
  'variables' => 
  array (
    'products' => 0,
    'product' => 0,
    'allProductsLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5b0feebcdb5483_29986028',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b0feebcdb5483_29986028')) {function content_5b0feebcdb5483_29986028($_smarty_tpl) {?><section class="featured-products clearfix">
  <h1 class="h1 products-section-title text-uppercase">
    Популярные товары
  </h1>
  <div class="products">
          
  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/men/1-1-hummingbird-printed-t-shirt.html#/1-razmer-s/8-cvet-belyj" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/2-home_default/hummingbird-printed-t-shirt.jpg"
            alt = "Hummingbird printed t-shirt"
            data-full-size-image-url = "http://poshta.test/2-large_default/hummingbird-printed-t-shirt.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/men/1-1-hummingbird-printed-t-shirt.html#/1-razmer-s/8-cvet-belyj">Hummingbird printed t-shirt</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Базовая цена</span>
                <span class="regular-price">28,20 руб.</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">22,56 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag discount">Цена снижена</li>
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                      <div class="variant-links">
      <a href="http://poshta.test/men/1-1-hummingbird-printed-t-shirt.html#/1-razmer-s/8-cvet-belyj"
       class="color"
       title="Белый"
       
       style="background-color: #ffffff"           ><span class="sr-only">Белый</span></a>
      <a href="http://poshta.test/men/1-2-hummingbird-printed-t-shirt.html#/1-razmer-s/11-cvet-chyornyj"
       class="color"
       title="Чёрный"
       
       style="background-color: #434A54"           ><span class="sr-only">Чёрный</span></a>
    <span class="js-count count"></span>
</div>
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/glavnaya/2-9-brown-bear-printed-sweater.html#/1-razmer-s" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/21-home_default/brown-bear-printed-sweater.jpg"
            alt = "Brown bear printed sweater"
            data-full-size-image-url = "http://poshta.test/21-large_default/brown-bear-printed-sweater.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/glavnaya/2-9-brown-bear-printed-sweater.html#/1-razmer-s">Hummingbird printed sweater</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Базовая цена</span>
                <span class="regular-price">42,36 руб.</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">33,89 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag discount">Цена снижена</li>
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/3-home_default/the-best-is-yet-to-come-framed-poster.jpg"
            alt = "The best is yet to come&#039; Framed poster"
            data-full-size-image-url = "http://poshta.test/3-large_default/the-best-is-yet-to-come-framed-poster.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm">The best is yet to come&#039;...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">34,22 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/glavnaya/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/4-home_default/the-adventure-begins-framed-poster.jpg"
            alt = "The adventure begins Framed poster"
            data-full-size-image-url = "http://poshta.test/4-large_default/the-adventure-begins-framed-poster.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/glavnaya/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm">The adventure begins Framed...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">34,22 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/5-home_default/today-is-a-good-day-framed-poster.jpg"
            alt = "Today is a good day Framed poster"
            data-full-size-image-url = "http://poshta.test/5-large_default/today-is-a-good-day-framed-poster.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm">Today is a good day Framed...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">34,22 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/home-accessories/6-mug-the-best-is-yet-to-come.html" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/6-home_default/mug-the-best-is-yet-to-come.jpg"
            alt = "Mug The best is yet to come"
            data-full-size-image-url = "http://poshta.test/6-large_default/mug-the-best-is-yet-to-come.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/home-accessories/6-mug-the-best-is-yet-to-come.html">Mug The best is yet to come</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">14,04 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/home-accessories/7-mug-the-adventure-begins.html" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/7-home_default/mug-the-adventure-begins.jpg"
            alt = "Mug The adventure begins"
            data-full-size-image-url = "http://poshta.test/7-large_default/mug-the-adventure-begins.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/home-accessories/7-mug-the-adventure-begins.html">Mug The adventure begins</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">14,04 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://poshta.test/glavnaya/8-mug-today-is-a-good-day.html" class="thumbnail product-thumbnail">
          <img
            src = "http://poshta.test/8-home_default/mug-today-is-a-good-day.jpg"
            alt = "Mug Today is a good day"
            data-full-size-image-url = "http://poshta.test/8-large_default/mug-today-is-a-good-day.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://poshta.test/glavnaya/8-mug-today-is-a-good-day.html">Mug Today is a good day</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Цена</span>
              <span itemprop="price" class="price">14,04 руб.</span>

              

              
            </div>
                  

        
          
        
      </div>

      
        <ul class="product-flags">
                      <li class="product-flag new">Новое</li>
                  </ul>
      

      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Быстрый просмотр
          </a>
        

        
                  
      </div>

    </div>
  </article>


      </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://poshta.test/2-glavnaya">
    Все товары<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }} ?>
